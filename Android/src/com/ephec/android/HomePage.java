package com.ephec.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ephec.android.advert.CreateAdvertPage;
import com.ephec.android.advert.OffresPage;
import com.ephec.android.advert.RequestPage;
import com.ephec.android.messageBox.MessageBox;
import com.ephec.utils.Utilities;

/**
 * Page d'accueil du programme une fois l'identification effectuée. Cette classe permet d'accéder au principales fonctionnalités de 
 * l'application au travers de boutons. Elle étend une activité et peut donc être lancée comme telle.
 * @author Peri
 *
 */
public class HomePage extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_page);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Fonction qui affiche la page des offres de l'utilisateur.
	 * @param view - source de l'évènement
	 */
	public void boutonOffres(View view){
    	Utilities.enter(OffresPage.class, this);
    }
	/**
	 * Fonction qui affiche la page des demandes de l'utilisateur 
	 * @param view - source de l'évènement
	 */
	public void boutonDemandes(View view){
    	Utilities.enter(RequestPage.class, this);
    }
	/**
	 * Fonction qui affiche la page profil de l'utilisateur 
	 * @param view - source de l'évènement
	 */
	public void boutonProfil(View view){
    	Utilities.enter(ProfilPage.class, this);
    }
	
	/**
	 * Fonction qui affiche la page de création d'offre
	 * @param view - source de l'évènement
	 */
	public void boutonCreerOffre(View view){
		Intent toLaunch = new Intent(this, CreateAdvertPage.class); //préparation de la page suivante  
		toLaunch.putExtra("mode", "offre");
    	this.startActivity(toLaunch);
	}
	
	/**
	 * Fonction qui affiche la page de création de demande
	 * @param view - source de l'évènement
	 */
	public void boutonCreerDemande(View view){
		Intent toLaunch = new Intent(this, CreateAdvertPage.class); //préparation de la page suivante  
		toLaunch.putExtra("mode", "demande");
    	this.startActivity(toLaunch);
	}	
	
	/**
	 * Fonction qui permet d'écrire un message
	 */
	public void boutonCreerMsg(View view){
		Utilities.enter(MessageBox.class, this);
	}
}

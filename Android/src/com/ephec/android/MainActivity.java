package com.ephec.android;

import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import com.ephec.utils.NetworkRequestAdapter;
import com.ephec.utils.Utilities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Classe principale du programme. C'est d'ici que nous lanceront notre application et que nous nous connecterons.
 * @author Peri
 *
 */
public class MainActivity extends Activity implements Observer{
	
	/**
	 * Champs servant à entrer ses identifiants
	 */
	private EditText fEmail, fMdp;
	
	/**
	 * Affiche à l'écran les erreurs
	 */
	private TextView error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        fEmail = (EditText) findViewById(R.id.email);
        fMdp = (EditText) findViewById(R.id.mdp);
        error = (TextView) findViewById(R.id.error);
        }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /**
     * Fonction permettant de tenter une connexion sur le serveur.
     * @param view - origine de l'évènement.
     */
    public void connection(View view){
    	error.setText("");
    	//condition de connection pour afficher l'erreur avant de lancer la Home_Page
    	//retour de la fonction pour la condition
    	connect(fEmail.getText().toString(), fMdp.getText().toString());   	
    }

    /**
     * Connexion au serveur php. La réponse arrivera grace à la méthode {@linkplain #update}.
     * @param email Email du compte
     * @param mdp Mot de passe du compte
     */
    private void connect(String email, String mdp){

		NetworkRequestAdapter net = new NetworkRequestAdapter(this);
    	net.addObserver(this);
    	
    	String address = getResources().getString(R.string.serveurAdd) 
    					+ getResources().getString(R.string.connexionAdd);
    	
    	net.setUrl(address);
    	net.addParam("email", email);
    	net.addParam("mdp", mdp);
    	
		net.send();
				
	    }

    @Override
	public void update(Observable observable,final Object msg) {
		if(msg==null) return ;
				
		if(msg.toString().equals(NetworkRequestAdapter.NO_ERROR)){
			Utilities.enter(HomePage.class, this);
			initAdvertTypesTable(observable);
		}
		else 
			error.setText(msg.toString());	
	}
    
    /**
     * En paralèle du thread principal, on exécute les méthodes remplissant les tables des codes d'erreurs et des catégories d'offres
     * @param observable
     */
    private void initAdvertTypesTable(final Observable observable){
    		Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
			    	try {
	    			Utilities.fillAdvertTable((JSONObject) ((NetworkRequestAdapter)observable).getResult().get("types"));	
	    			Utilities.fillErrorsTable((JSONObject) ((NetworkRequestAdapter)observable).getResult().get("errors"));	
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});
    		
    		t.start();
		
    }
}

package com.ephec.android.advert;

import java.util.Observable;
import java.util.Observer;

import org.apache.http.message.BasicNameValuePair;

import com.ephec.android.R;
import com.ephec.android.advert.list.L4HJSONConverter;
import com.ephec.android.advert.list.Advert;
import com.ephec.android.dialog.AdvertFragment;
import com.ephec.utils.NetworkRequestAdapter;
import com.ephec.utils.Utilities;

import static com.ephec.utils.NetworkRequestAdapter.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Classe affichant les listes d'annonces. 
 * Note : quand on place l'activité en pause, elle est killed d'office, cela permet de recharcher une liste plus actuelle dans 
 * le cas ou la page est en pause longtemps. Voir {@linkplain #onPause}.
 * @author Peri
 *
 */
public abstract class AdvertPage extends Activity implements Observer, OnItemClickListener{
	
	/**
	 * Liste pouvant contenir des annonces
	 */
	private ListView list;
	
	/**
	 * La réponse a une annonce a bien été enregistrée
	 */
	public final static int ANSWERED = 5;
	
	/**
	 * Envois de réponse a une annonce
	 */
	public final static int ANS_ADVERT = 4;
	
	/**
	 * Page des offres personnelles
	 */
	public final static int MY_REQUEST = 3;
	
	/**
	 * Page des demandes
	 */
	public final static int REQUEST = 2;
	
	/**
	 * Page des offres personnelles
	 */
	public final static int MY_OFFRES = 1;
	
	/**
	 * Page des offres
	 */
	public final static int OFFRES = 0;
	
	private int swapMenu;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_advert_page);
		
		list = (ListView)findViewById(R.id.orderList);
		list.setOnItemClickListener(this);
		putTitle();
		askListView(getMode());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.advert_page, menu);
		menu.add(getTitleString()).setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS).getItemId();
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		if (id == R.id.action_filter) {
			showFilterDialog();
			return true;
		}
		
		if(id == swapMenu){
			Utilities.enter(switchMode(), this);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Demande la liste des annonces au serveur
	 * On sait si il s'agit d'offre ou de demande en fonction de la classe fille qui aura implémenté {@linkplain #getMode()}.
	 * @param mode type de liste
	 */
	protected void askListView(int mode){
		String url = getResources().getString(R.string.serveurAdd) 
					+ getResources().getString(R.string.advertsListAdd);
		
		NetworkRequestAdapter net = new NetworkRequestAdapter(this, url);
		net.addObserver(this);
		net.addParam(new BasicNameValuePair("mode", String.valueOf(mode)));
		net.send();
	}
	
	/**
	 * Demande la liste des annonces filtrée au serveur
	 * On sait si il s'agit d'offre ou de demande en fonction de la classe fille qui aura implémenté {@linkplain #getMode()}.
	 * @param filters les filtres au format int. Les filtres sont les id des tags.
	 * @param mode type de liste
	 */
	protected void askFilteredListView(String filters, int mode){
		String url = getResources().getString(R.string.serveurAdd) 
					+ getResources().getString(R.string.advertsListAdd);
		
		NetworkRequestAdapter net = new NetworkRequestAdapter(this, url);
		net.addObserver(this);
		net.addParam(new BasicNameValuePair("mode", String.valueOf(mode)));
		net.addParam(new BasicNameValuePair("filters", filters));
		net.send();
	}
	
	/**
	 *Place le titre de l'activité en haut de la page
	 */
	protected abstract void putTitle();
	
	/**
	 * Place le titre de l'activité en haut de la page
	 * @param title la ressource
	 */
	protected void putTitle(int title){
		((TextView)findViewById(R.id.title_advertList)).setText(
				getResources().getString(title));
	}
	
	protected abstract String getTitleString();
	
	@Override
	public void onPause(){
		super.onPause();
		finish();
	}
	
	abstract boolean isAnswerable(long id);
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(isAnswerable(id)){
			DialogFragment dialog = new AdvertFragment((Advert) parent.getItemAtPosition(position), this);
			dialog.show(getFragmentManager(), "advert");
		}
	}
	
	/**
	 * Répondre a une annonce
	 * @param id de l'annonce concernée
	 */
	public void answerAdvert(long id){
		String url = getResources().getString(R.string.serveurAdd) 
				+ getResources().getString(R.string.advertUtil);
	
		NetworkRequestAdapter net = new NetworkRequestAdapter(this, url);
		net.addObserver(this);
		net.addParam(new BasicNameValuePair("mode", String.valueOf(ANS_ADVERT)));
		net.addParam(new BasicNameValuePair("advertId", String.valueOf(id)));
		net.send();
	}
	
	/**
	 * Renvoie le mode de la page.
	 *  ({@linkplain #OFFRES} | {@linkplain #REQUEST} | {@linkplain #MY_OFFRES} | {@linkplain #MY_REQUEST}}
	 * @return
	 */
	protected abstract int getMode();
	
	@Override
	public void update(Observable observable, Object data) {
		if(!data.toString().startsWith(String.valueOf(ERROR))){
			int resultCode;
			try {
				resultCode = Integer.parseInt(data.toString());
			} catch (NumberFormatException e) {
				resultCode = ERROR;
				e.printStackTrace();
			}
			
			if(resultCode==OK)
				new L4HJSONConverter(this,list).execute(((NetworkRequestAdapter)observable).getResult());
			if(resultCode == ANSWERED)
				Utilities.agreed(R.string.adDialog_title
						, R.string.adDialog_message
						, this);
				
		}
		else{
			Utilities.showErrorMessages((NetworkRequestAdapter)observable, this);
		}
		
	}	
	
	public abstract Class<? extends AdvertPage> switchMode();
	
	/**
	 * Affiche une liste de filtres dans laquelle on peut choisir une valeur.
	 * La valeur est envoyée au serveur qui va renvoyer une nouvelle liste triée
	 */
	public void showFilterDialog(){
		new AlertDialog.Builder(this)
	    .setTitle(getResources().getString(R.string.title_filters))
	    .setSingleChoiceItems(Utilities.getAdvertTypesString(), -1, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				askFilteredListView("" + Utilities.getAdvertTypes().get(
					Utilities.getAdvertTypesString()[which])
					,getMode());
					dialog.dismiss();
			}
		})
	    .show();
	}
	
	
	
}

package com.ephec.android.advert;

import com.ephec.android.R;

/**
 * Spacialistion de la classe {@linkplain AdvertPage} pour afficher les Offres et faire éventuellement des traitements spécifiques
 * @author Peri
 *
 */
public class OffresPage extends AdvertPage {

	@Override
	protected int getMode() {
		return OFFRES;
	}

	@Override
	protected void putTitle() {
		putTitle(R.string.title_offres_list);		
	}

	@Override
	public Class<? extends AdvertPage> switchMode() {
		return MyOffersPage.class;
	}

	@Override
	protected String getTitleString() {
		return getResources().getString(R.string.title_my_offres_list);
	}

	@Override
	boolean isAnswerable(long id) {
		return true;
	}
}

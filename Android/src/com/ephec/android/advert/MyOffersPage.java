package com.ephec.android.advert;

import com.ephec.android.R;

public class MyOffersPage extends AdvertPage {

	@Override
	protected void putTitle() {
		putTitle(R.string.title_my_offres_list);
	}
	
	@Override
	boolean isAnswerable(long id) {
		return false;
	}

	@Override
	protected int getMode() {
		return MY_OFFRES;
	}

	@Override
	public Class<? extends AdvertPage> switchMode() {
		return OffresPage.class;
	}

	@Override
	protected String getTitleString() {
		return getResources().getString(R.string.title_offres_list);
	}

}

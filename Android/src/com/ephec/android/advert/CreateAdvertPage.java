package com.ephec.android.advert;

import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import com.ephec.android.R;
import com.ephec.android.dialog.DatePickerFragment;
import com.ephec.android.dialog.TimePickerFragment;
import com.ephec.utils.NetworkRequestAdapter;
import com.ephec.utils.Utilities;

import static com.ephec.utils.NetworkRequestAdapter.*;
import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Page de création d'une advert. La page envoie l'advert après un controle minimum de ses champs puis, si le serveur répond,
 * la méthode update s'occupe de réceptionner la réponse et de lancer les traitements.
 * @author Peri
 *
 */
public class CreateAdvertPage extends Activity implements Observer {
	private EditText ftitre, fdescription;
	private TextView fdateExp,fheureExp;
	private Spinner ftype;
	private String mode;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_advert_page);
		mode = getIntent().getExtras().get("mode").toString();

		ftitre = (EditText) findViewById(R.id.advert_Titre);
		fdescription = (EditText) findViewById(R.id.advert_Description);
		fdateExp = (TextView) findViewById(R.id.date_advert);
		fheureExp = (TextView) findViewById(R.id.heure_advert);
		
		fheureExp.setText(DateFormat.format("hh:mm:ss", new Date()));
		
		
		final Calendar c = Calendar.getInstance();
	    int yy = c.get(Calendar.YEAR);
	    int  mm = c.get(Calendar.MONTH);
	    int dd = c.get(Calendar.DAY_OF_MONTH);

	    fdateExp.setText(new StringBuilder()
	            .append(yy).append(" ").append("-").append(mm + 1).append("-")
	            .append(dd+13));
		
		
		

		// selection du type d'advert
		ftype = (Spinner) findViewById(R.id.type_advert_spinner);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item,
				Utilities.getAdvertTypesString());
		ftype.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_advert_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void showTimePickerDialog(View v) {
		TextView heure = ((TextView)findViewById(R.id.heure_advert));
		DialogFragment newFragment = new TimePickerFragment(heure);
		newFragment.show(getFragmentManager(), "timePicker");
		fheureExp=heure;
	}
	
	
	public void showDatePickerDialog(View v) {
		TextView date=((TextView)findViewById(R.id.date_advert));
		DialogFragment newFragment = new DatePickerFragment(date);
		newFragment.show(getFragmentManager(), "datePicker");
		fdateExp=date;
	}

	/**
	 * Envoie l'advert au serveur.
	 * Methode appelée par un bouton de la page
	 * @param view
	 */
	public void EnvoyerAdvert(View view){
		//traitement de l'envoie de l'advert
		sendAdvert(ftitre.getText().toString(), fdescription.getText().toString(),
				fdateExp.getText().toString(), fheureExp.getText().toString(),
				String.valueOf(ftype.getSelectedItemId()));

	}

	/**
	 * Envoie l'advert au serveur.
	 * @param titre
	 * @param description
	 * @param date au bon format YYYY-mm-DD
	 * @param heure au bon format HH:MM:SS
	 * @param type un int au format string
	 */
	public void sendAdvert(String titre,String description,String date, String heure, String type){
		int nError=0;
		NetworkRequestAdapter net = new NetworkRequestAdapter(this);
		net.addObserver(this);

		String address = getResources().getString(R.string.serveurAdd) 
				+ getResources().getString(R.string.advertUtil);
		net.setUrl(address);

		if(titre != null && titre.length()>0)
			net.addParam("titre", titre);
		else nError++;

		if(description != null && description.length()>0)
			net.addParam("description", description);
		else nError++;
		if(type!=null && type.length()>0)
			net.addParam("tagId", type);
		else nError++;
		if(date!=null && heure !=null)
			net.addParam("time", date+" "+heure);
		
		net.addParam("mode", mode);
		
		if(nError==0)
			net.send();
		else 
			((TextView)findViewById(R.id.error)).setText("Un ou plusieurs champs sont vides");
	}

	@Override
	public void update(Observable observable, Object data) {
		if(data.toString().startsWith("2")){
			Utilities.agreed( R.string.adDialog_valid
							, R.string.adDialog_message
							, this);
		}
		else{
			Utilities.showErrorMessages((NetworkRequestAdapter) observable, this);
		}
	}
}

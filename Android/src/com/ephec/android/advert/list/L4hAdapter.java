package com.ephec.android.advert.list;

import java.util.List;

import com.ephec.android.R;

import android.app.Activity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Adapter pour gérer une liste d'offre.
 * @author Peri
 *
 */
public class L4hAdapter extends BaseAdapter{
	
	static class ViewHolder {
	    public TextView orderTitle;
	    public TextView orderDate;
	    public TextView orderDescription;
	    public TextView orderAuthor;
	  }
	
	/**
	 * Liste stoquant les offres
	 */
	private List<Advert> data;
	
	/**
	 * Activity ayant créé cet adapter
	 */
	private Activity client;
	
	/**
	 * Constructor
	 * @param context la source
	 * @param data Liste des adverts
	 */
	public L4hAdapter(Activity context, List<Advert> data) {
		super();
		this.data = data;
		this.client = context;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
	    // reuse views
	    if (rowView == null) {
	      LayoutInflater inflater = client.getLayoutInflater();
	      rowView = inflater.inflate(R.layout.rowlayout, null);
	      // configure view holder
	      ViewHolder viewHolder = new ViewHolder();
	      viewHolder.orderTitle = (TextView) rowView.findViewById(R.id.orderTitle);
	      viewHolder.orderDate = (TextView) rowView.findViewById(R.id.orderDate);
	      viewHolder.orderDescription = (TextView) rowView.findViewById(R.id.orderDescription);
	      viewHolder.orderAuthor = (TextView) rowView.findViewById(R.id.orderAuthor);
	      rowView.setTag(viewHolder);
	    }
	    
	    ViewHolder holder = (ViewHolder) rowView.getTag();
	    holder.orderDate.setText(DateFormat.format("dd/MM/yyyy", data.get(position).getDate()));
	    holder.orderDescription.setText(data.get(position).getDescription());
	    holder.orderTitle.setText(data.get(position).getTitle());
	    holder.orderAuthor.setText(data.get(position).getAuthor());
	    
	    
		return rowView;
	}
	
}
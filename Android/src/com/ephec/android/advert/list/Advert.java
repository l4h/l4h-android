package com.ephec.android.advert.list;

import java.util.Date;

/**
 * Classe représentant une Annonce
 * @author Peri
 *
 */
public class Advert {
	
	/**
	 * Id dans la base de donnée
	 */
	private long id;
	
	/**
	 * Auteur de l'annonce
	 */
	private String author;
	/**
	 * Titre
	 */
	private String title;
	/**
	 * Description
	 */
	private String description;
	/**
	 * Date d'expiration
	 */
	private Date date;
	/**
	 * Ville où se trouve l'auteur
	 */
	private String city;
	
	public Advert(){}
	
	public Advert(long id, String name, String description, Date date) {
		super();
		this.title = name;
		this.description = description;
		this.date = date;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	

}

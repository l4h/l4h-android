package com.ephec.android.advert.list;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ListView;

/**
 * Convertisseur permettant d'adapter la liste venant de la base de donnée au format de liste android.
 * @author Peri
 *
 */
public class L4HJSONConverter extends AsyncTask<JSONObject, Void, L4hAdapter>{

	private Activity client;
	private ListView list;
	public L4HJSONConverter(Activity client, ListView view) {
		this.client = client;
		list = view;
	}
	
	@Override
	protected L4hAdapter doInBackground(JSONObject... params) {
		JSONObject map = params[0];
		
		List<Advert> list = new ArrayList<Advert>(); //liste contenant les annonces
		JSONArray keys = map.names();
		
			if (keys != null) {
				for (int i = 0; i < keys.length(); i++) { //on liste les clés pour le remplissage d'un nouvel objet 
						try {
							Object json = map.get(keys.get(i).toString());
							if (json instanceof JSONObject) {
								Advert o = new Advert();
								fillWithFields(o, (JSONObject)json);
								list.add(o);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}					
				}
			}
		
		return new L4hAdapter(client, list); // creation d'un adapteur de liste
	}
	
	/**
	 * Complete l'Advert en paramettre avec les info stoquée dans le Json object
	 * @param o destination
	 * @param json source
	 */
	private void fillWithFields(Advert o, JSONObject json) {
		
		try {			
			o.setAuthor(json.getString("auteur"));
			o.setId(json.getLong("id"));
			o.setTitle(json.getString("titre"));
			o.setCity(json.getString("ville"));
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH); //format de conversion de la date
			try {
				o.setDate(df.parse(json.getString("dateExpiration")));
			} catch (ParseException e) {
				o.setDate(new Date());
				e.printStackTrace();
			}
			
			o.setDescription(json.getString("advert"));
		} catch (JSONException e) {
			e.printStackTrace();
		}		
	}

	@Override
	protected void onPostExecute(L4hAdapter list){
		this.list.setAdapter(list); //Affectation a la liste de l'adapteur
	}
}

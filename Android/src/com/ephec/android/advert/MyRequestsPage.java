package com.ephec.android.advert;

import com.ephec.android.R;

public class MyRequestsPage extends AdvertPage {

	@Override
	protected void putTitle() {
		putTitle(R.string.title_my_requests_list);
	}

	@Override
	protected int getMode() {
		return MY_REQUEST;
	}

	@Override
	public Class<? extends AdvertPage> switchMode() {
		return RequestPage.class;
	}

	@Override
	protected String getTitleString() {
		return getResources().getString(R.string.title_requests_list);
	}

	@Override
	boolean isAnswerable(long id) {
		return false;
	}

}

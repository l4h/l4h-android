package com.ephec.android.advert;

import com.ephec.android.R;

/**
 * Spacialistion de la classe {@linkplain AdvertPage} pour afficher les Demandess et faire éventuellement des traitements spécifiques
 * @author Peri
 *
 */
public class RequestPage extends AdvertPage {

	@Override
	protected int getMode() {
		return REQUEST;
	}

	@Override
	protected void putTitle() {
		putTitle(R.string.title_requests_list);			
	}

	@Override
	public Class<? extends AdvertPage> switchMode() {
		return MyRequestsPage.class;
	}

	@Override
	protected String getTitleString() {
		return getResources().getString(R.string.title_my_requests_list);
	}

	@Override
	boolean isAnswerable(long id) {
		return true;
	}
}

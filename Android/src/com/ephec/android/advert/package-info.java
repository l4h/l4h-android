/**
 * Rassemble toutes les classes relatives aux annonces. Elles permettent, entre autre, d'afficher les annonces ou les modifier.
 * @author Peri
 *
 */
package com.ephec.android.advert;
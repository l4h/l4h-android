package com.ephec.android;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.ephec.utils.NetworkRequestAdapter;

/**
 * Activité d'affichage et modification du profil.
 * @author Steeve
 *
 */
public class ProfilPage extends Activity implements Observer {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profil_page);
		afficheProfil();
		
		Button boutonAlter =(Button)findViewById(R.id.modifier_profil);
		Button boutonSend = (Button)findViewById(R.id.envoyer_modifier_profil);
		
		final EditText fnom = (EditText) findViewById(R.id.hidden_edit_nom);
		final EditText fprenom = (EditText) findViewById(R.id.hidden_edit_prenom);
		final EditText fnumero = (EditText) findViewById(R.id.hidden_edit_numero);
		final EditText frue = (EditText) findViewById(R.id.hidden_edit_rue);
		final EditText fcodePostal = (EditText) findViewById(R.id.hidden_edit_CodePostal);
		final EditText fville = (EditText) findViewById(R.id.hidden_edit_ville);
		final EditText fpays = (EditText) findViewById(R.id.hidden_edit_pays);
		
		
		final ViewSwitcher switcher = (ViewSwitcher) findViewById(R.id.my_switcher);
		final ViewSwitcher switcher2 = (ViewSwitcher) findViewById(R.id.my_switcher2);
		final ViewSwitcher switcher3 = (ViewSwitcher) findViewById(R.id.my_switcher3);
		final ViewSwitcher switcher4 = (ViewSwitcher) findViewById(R.id.my_switcher4);
		final ViewSwitcher switcher5 = (ViewSwitcher) findViewById(R.id.my_switcher5);
		final ViewSwitcher switcher6 = (ViewSwitcher) findViewById(R.id.my_switcher6);
		final ViewSwitcher switcher7 = (ViewSwitcher) findViewById(R.id.my_switcher7);
		final ViewSwitcher switcher8 = (ViewSwitcher) findViewById(R.id.my_switcher8);
		
		boutonAlter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switcher.showNext();
				switcher2.showNext();
				switcher3.showNext();
				switcher4.showNext();
				switcher5.showNext();
				switcher6.showNext();
				switcher7.showNext();
				switcher8.showNext();
				
			}
		});
		boutonSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Envoyer les modifications
				sendNewProfil(fnom.getText().toString(), fprenom.getText().toString(),frue.getText().toString(), 
						fnumero.getText().toString(), 
						fcodePostal.getText().toString(), fville.getText().toString(), fpays.getText().toString());
				
				switcher.showPrevious();
				switcher2.showPrevious();
				switcher3.showPrevious();
				switcher4.showPrevious();
				switcher5.showPrevious();
				switcher6.showPrevious();
				switcher7.showPrevious();
				switcher8.showPrevious();
			
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profil_page, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		if (id == R.id.action_settings) {
			
			return true;
		}
		return super.onOptionsItemSelected(item);
		
	}

	private void sendNewProfil( String nom, String prenom, String rue,String numero,String codeP,String ville, String pays){
		int nError=0;
		NetworkRequestAdapter net = new NetworkRequestAdapter(this);
		net.addObserver(this);
		String address = getResources().getString(R.string.serveurAdd) 
				+ getResources().getString(R.string.pageModifProfil);
		net.setUrl(address);
		if(nom != null && nom.length()>0)
		net.addParam("nom", nom);
		else nError++;
		
		if(prenom!= null && prenom.length()>0)
		net.addParam("prenom", prenom);
		else nError++;
		
		if(rue != null && rue.length()>0)
		net.addParam("rue", rue);
		else nError++;
		
		if(numero != null && numero.length()>0)
			net.addParam("numero", numero);
			else nError++;
		
		if(codeP != null && codeP.length()>0)
		net.addParam("codePostal", codeP);
		else nError++;
		
		if(ville != null && ville.length()>0)
		net.addParam("ville", ville);
		else nError++;
		
		if(pays != null && pays.length()>0)
		net.addParam("pays", pays);
		else nError++;
		
		if(nError==0)
			net.send();
		else 
			((TextView)findViewById(R.id.error)).setText("Un ou plusieurs champs sont vides");
		
	
	}


	private void afficheProfil(){

		NetworkRequestAdapter net = new NetworkRequestAdapter(this);
		net.addObserver(this);
		String address = getResources().getString(R.string.serveurAdd) 
				+ getResources().getString(R.string.pageProfil);
		net.setUrl(address);
		net.send();


	}
	
	
	


	@Override

	public void update(Observable observable, Object data) {
		NetworkRequestAdapter resultat=((NetworkRequestAdapter)observable);
		if(data.toString().equals("" + NetworkRequestAdapter.OK)){
			
			try{
				((TextView)findViewById(R.id.profil_Prenom)).setText(resultat.getResult().get("prenom").toString());
				((EditText)findViewById(R.id.hidden_edit_prenom)).setText(resultat.getResult().get("prenom").toString());
				((TextView)findViewById(R.id.profil_nom)).setText(resultat.getResult().get("nom").toString());
				((EditText)findViewById(R.id.hidden_edit_nom)).setText(resultat.getResult().get("nom").toString());
				
			}catch (JSONException e){
				e.printStackTrace();
			}
			
			try {
				
				
				((TextView)findViewById(R.id.profil_Email)).setText(resultat.getResult().get("email").toString());
				((TextView)findViewById(R.id.profil_Rue)).setText(resultat.getResult().get("rue").toString());
				((EditText)findViewById(R.id.hidden_edit_rue)).setText(resultat.getResult().get("rue").toString());
				
				((TextView)findViewById(R.id.profil_Numero)).setText(resultat.getResult().get("numero").toString());
				((EditText)findViewById(R.id.hidden_edit_numero)).setText(resultat.getResult().get("numero").toString());
				
				((TextView)findViewById(R.id.profil_CodePostal)).setText(resultat.getResult().get("codePostal").toString());
				((EditText)findViewById(R.id.hidden_edit_CodePostal)).setText(resultat.getResult().get("codePostal").toString());
				
				((TextView)findViewById(R.id.Profil_Ville)).setText(resultat.getResult().get("ville").toString());
				((EditText)findViewById(R.id.hidden_edit_ville)).setText(resultat.getResult().get("ville").toString());
				
				((TextView)findViewById(R.id.profil_pays)).setText(resultat.getResult().get("pays").toString());
				((EditText)findViewById(R.id.hidden_edit_pays)).setText(resultat.getResult().get("pays").toString());
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}



	}

}

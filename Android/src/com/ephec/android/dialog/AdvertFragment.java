package com.ephec.android.dialog;

import com.ephec.android.R;
import com.ephec.android.advert.AdvertPage;
import com.ephec.android.advert.list.Advert;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Fragment présentant une annonce
 * @author Peri
 *
 */
public class AdvertFragment extends DialogFragment{
	
	private Advert advert;
	private AdvertPage source;
	
	public AdvertFragment(Advert o, AdvertPage source) {
		super();
		if(o instanceof Advert)
			this.advert = o;
		this.source = source;
	}
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View v = inflater.inflate(R.layout.fragment_advert, null);
        builder.setView(v).setMessage(advert.getTitle())
               .setPositiveButton(R.string.advertFrag_answer, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       source.answerAdvert(advert.getId());
                   }
               })
               .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       dismiss();
                   }
               });
        insertAdvert(v);
        
        // Create the AlertDialog object and return it
        return builder.create();
    }
	
	/**
	 * Complete les champs du fragment fragment
	 * @param v
	 */
	private void insertAdvert(View v){
		((TextView)v.findViewById(R.id.dialogAdPseudo)).setText(advert.getAuthor());
		((TextView)v.findViewById(R.id.dialogAdDate)).setText(DateFormat.format("dd/MM/yyyy",advert.getDate()));
		((TextView)v.findViewById(R.id.dialogAdLocation)).setText(advert.getCity());
		((TextView)v.findViewById(R.id.dialogAdDescription)).setText(advert.getDescription());
	}
}

package com.ephec.android.dialog;

import java.util.Calendar;




import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;

import android.widget.TextView;
import android.widget.TimePicker;

/**
 * Classe servant à afficher un dialog pour choisir une heure.
 * @author Steeves
 *
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
	
	/**
	 * View a remplire avec la heure choisie
	 */
	private TextView o;
	
	public TimePickerFragment(TextView o){
		this.o = o;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
        
        
    }

	@Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        o.setText(hourOfDay + ":" + minute+":"+"00");
    }
    
  
	

}

/**
 * Package regroupant les dialog qui s'affichent au dessus des {@linkplain android.app.Activity}
 * @author Peri
 *
 */
package com.ephec.android.dialog;
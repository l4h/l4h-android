package com.ephec.android.messageBox;

import java.util.Observable;
import java.util.Observer;

import com.ephec.android.R;
import com.ephec.utils.NetworkRequestAdapter;
import com.ephec.utils.Utilities;

import static com.ephec.utils.NetworkRequestAdapter.*;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MessageBox extends Activity implements Observer{
	
	private EditText dest, title, content;
	public static int SEND = 1;
	public static int GET = 2;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_box);
		
		if(getIntent().getStringExtra("dest") != null)
			((EditText)findViewById(R.id.msgBox_dest)).setText(//set un dest
								getIntent().getStringExtra("dest"));//get le destinataire de la page appelant celle ci
		
		dest = (EditText)findViewById(R.id.msgBox_dest);
		content = (EditText)findViewById(R.id.msgBox_content);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.message_box, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void sendMsg(View view){
		NetworkRequestAdapter net = new NetworkRequestAdapter(this);
		
		String address = getResources().getString(R.string.serveurAdd) 
				+ getResources().getString(R.string.sendMsg);

		net.setUrl(address);
		net.addParam("mode", String.valueOf(SEND));
		net.addParam("dest", dest.getText().toString());
		net.addParam("title", title.getText().toString());
		net.addParam("content", content.getText().toString());
		
	}

	@Override
	public void update(Observable observable, Object data) {
		if(!data.toString().startsWith(String.valueOf(ERROR))){
			int resultCode;
			try {
				resultCode = Integer.parseInt(data.toString());
			} catch (NumberFormatException e) {
				resultCode = ERROR;
				e.printStackTrace();
			}
			
			if(resultCode == OK)
				;
			else{
				Utilities.showErrorMessages((NetworkRequestAdapter)observable, this);
			}
		}
	}
}
